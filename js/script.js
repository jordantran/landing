$( document ).ready(function(){
    $('.news-slider').owlCarousel({
        loop:true,
        margin:20,
        nav:false,
        dots:false,
        responsive:{
            0:{
                items:1
            },
            480:{
                items:2
            },
            767:{
                items:3
            }
        }
     });
    $('.news-update .customNextBtn').click(function() {
        $('.news-slider').trigger('next.owl.carousel');
      });
    $('.news-update .customPreviousBtn').click(function() {
        $('.news-slider').trigger('prev.owl.carousel');
    });

    $('.game-slider').owlCarousel({
        loop:true,
        margin:20,
        nav:false,
        dots:false,
        items:1
     });
    
    $('.game-play .customNextBtn').click(function() {
        $('.game-slider').trigger('next.owl.carousel');
      });
    $('.game-play .customPreviousBtn').click(function() {
        $('.game-slider').trigger('prev.owl.carousel');
    });

    $('.heroes-slider').owlCarousel({
        loop:true,
        margin:20,
        nav:false,
        dots:false,
        responsive:{
            0:{
                items:3
            },
            480:{
                items:5
            }
            
        }
     });
    $('.game-heroes .customNextBtn').click(function() {
        $('.heroes-slider').trigger('next.owl.carousel');
      });
    $('.game-heroes .customPreviousBtn').click(function() {
        $('.heroes-slider').trigger('prev.owl.carousel');
    });
})

